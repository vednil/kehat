# kehat

Mail server based on open source software:
- Postfix
- Dovecot
- OpenDKIM
- OpenDMARC
- Amavis
- ClamAV
- SpamAssassin
- PostfixAdmin
- Roundcube

Run mail server with Docker Compose (before change domain name and database passwords in docker-compose.yml):
```
docker-compose up -d
```
