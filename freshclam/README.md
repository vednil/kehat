# Quick reference

- Maintained by: https://gitlab.com/vednil/kehat

# What is it?

freshclam is a virus database update tool for ClamAV. This image update virus database and notify remote ClamAV.

# How to use this image

```
$ docker run -d \
        -e CLAMAV_HOST=clamav \
        -v clamav:/var/lib/clamav  \
        vednil/freshclam
```

Note: A directory `/var/lib/clamav` used to store ClamAV virus database. Share this directory with ClamAV (e.g. use `vednil/clamav` image).

## Environment Variables

### `CLAMAV_HOST`

ClamAV IP address or DNS name. Use to notify remote ClamAV about new virus database after update.

