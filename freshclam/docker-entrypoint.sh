#!/usr/bin/env bash

echo "TCPAddr $CLAMAV_HOST" > /etc/clamav/clamd.conf
echo "TCPSocket 3310" >> /etc/clamav/clamd.conf

chown -R clamav:clamav /var/lib/clamav


exec "$@"
