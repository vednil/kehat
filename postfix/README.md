# Quick reference

- Maintained by: https://gitlab.com/vednil/kehat

# What is it?

Postfix is an open source mail transfer agent (MTA) that send and receive mail.

# How to use this image

Run Postfix with self-signed certificate:

```
$ docker run -d \
        -p 25:25 \
        -p 587:587 \
        -e DOMAIN=mail.example.com \
        -e DOVECOT_HOST=dovecot \
        -e AMAVIS_HOST=amavis \
        -e OPENDKIM_HOST=opendkim \
        -e OPENDMARC_HOST=opendmarc \
        -e DB_HOST=db \
        -e DB_USER=postfix \
        -e DB_PASSWORD=password \
        -e DB_NAME=postfix \
        -v postfix:/etc/postfix/private  \
        vednil/postfix
```

Note: A directory `/etc/postfix/private` used to store self-signed certificate and Diffie-Hellman key. Certificate will be created at container startup.

Run Postfix with your existing Let's Encrypt certificate:

```
$ docker run -d \
        -p 25:25 \
        -p 587:587 \
        -e DOMAIN=mail.example.com \
        -e DOVECOT_HOST=dovecot \
        -e AMAVIS_HOST=amavis \
        -e OPENDKIM_HOST=opendkim \
        -e OPENDMARC_HOST=opendmarc \
        -e DB_HOST=db \
        -e DB_USER=postfix \
        -e DB_PASSWORD=password \
        -e DB_NAME=postfix \
        -e SMTPD_TLS_CHAIN_FILES="/etc/letsencrypt/live/mail.example.com/privkey.pem /etc/letsencrypt/live/mail.example.com/fullchain.pem" \
        -v /etc/letsencrypt/live/mail.example.com:/etc/letsencrypt/live/mail.example.com:ro  \
        -v /etc/letsencrypt/archive/mail.example.com:/etc/letsencrypt/archive/mail.example.com:ro \
        vednil/postfix sh -c 'while :; do sleep 6h & wait ${!}; postfix check && postfix reload; done & postfix start-fg'
```

Note: Postfix don't get or renew Let's Encrypt certificate (e. g. you can use nginx and certbot to get and renew certificate). This image run with custom command to apply `postfix reload` every 6 hours because it's required after renewing certificate.

## Environment Variables

### `DOMAIN`

This environment variable is used to set `myhostname` Postfix parameter.

### `DOVECOT_HOST`

Dovecot IP address or DNS name. Perform mail delivery and user authentification.

### `AMAVIS_HOST`

Amavis IP address or DNS name. Check virus and spam in mail.

### `OPENDKIM_HOST`

OpenDKIM IP address or DNS name. Sign outgoing and check incoming mail.

### `OPENDMARC_HOST`

OpenDMARC IP address or DNS name. Check DMARC record.

### `DB_HOST`

MariaDB IP address or DNS name. Database store domain and mailbox names.

### `DB_USER`

Database user name.

### `DB_PASSWORD`

Database user password.

### `DB_NAME`

Database name.

### `SMTPD_TLS_CHAIN_FILES`

This optional environment variable is used to set `smtpd_tls_chain_files` Postfix parameter. Use this variable to use your existing certificate in Postfix. List one or more PEM files, e. g. private key and certificate chain.

