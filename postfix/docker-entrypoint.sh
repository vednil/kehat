#!/usr/bin/env bash

# TLS certificate
if [ -z "$SMTPD_TLS_CHAIN_FILES" ]; then
    if [ ! -f /etc/postfix/private/$DOMAIN.key ] && [ ! -f /etc/postfix/private/$DOMAIN.pem ] ;then
        # Create Self-Signed Certificate
        openssl req -nodes -x509 -newkey rsa:4096 -subj "/CN=$DOMAIN" -keyout /etc/postfix/private/$DOMAIN.key -out /etc/postfix/private/$DOMAIN.pem -days 3650
        chmod 700 /etc/postfix/private
    fi
else
    # Use existing user certificate
    postconf -e "smtpd_tls_chain_files = $SMTPD_TLS_CHAIN_FILES"
fi

# Create DH key
if [ ! -f /etc/postfix/private/dh.pem ]; then
    openssl dhparam -out /etc/postfix/private/dh.pem 2048
fi

# Create database /etc/aliases.db
if [ ! -f /etc/aliases.db ]; then
    newaliases
fi

# Change myhostname parameter to user domain
postconf -e "myhostname = $DOMAIN"

# Deliver mail to Dovecot 
postconf -e "virtual_transport = lmtp:inet:[${DOVECOT_HOST}]:24"

# Authenticate user with Dovecot
postconf -e "smtpd_sasl_path = inet:[${DOVECOT_HOST}]:12345"

# Check mail for viruses
postconf -e "content_filter = amavisfeed:[${AMAVIS_HOST}]:10024"

# DKIM and DMARC
postconf -e "smtpd_milters = inet:[${OPENDKIM_HOST}]:8891, inet:[${OPENDMARC_HOST}]:8893"

# Mailbox and message size limits
if [ ! -z "$MAILBOX_SIZE_LIMIT" ]; then
    postconf -e "mailbox_size_limit = $MAILBOX_SIZE_LIMIT"
fi
if [ ! -z "$MESSAGE_SIZE_LIMIT" ]; then
    postconf -e "message_size_limit = $MESSAGE_SIZE_LIMIT"
fi

# DB parameters
sed -i "s/user =.*/user = ${DB_USER}/g" /etc/postfix/sql/*
sed -i "s/password =.*/password = ${DB_PASSWORD}/g" /etc/postfix/sql/*
sed -i "s/hosts =.*/hosts = ${DB_HOST}/g" /etc/postfix/sql/*
sed -i "s/dbname =.*/dbname = ${DB_NAME}/g" /etc/postfix/sql/*


exec "$@"
