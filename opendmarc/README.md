# Quick reference

- Maintained by: https://gitlab.com/vednil/kehat

# What is it?

OpenDMARC is a DMARC (Domain-based Message Authentication, Reporting and Conformance) verify software.

# How to use this image

```
$ docker run -d vednil/opendmarc
```

