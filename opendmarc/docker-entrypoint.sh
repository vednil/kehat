#!/usr/bin/env bash

# Delete rsyslog pid file on container restart
rm -f /var/run/rsyslogd.pid


exec "$@"