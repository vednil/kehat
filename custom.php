<?php

$config['imap_conn_options'] = array(
  'ssl' => array(
    'verify_peer_name'   => false,
    'allow_self_signed'  => true,
  ),
);

$config['smtp_conn_options'] = array(
  'ssl' => array(
    'verify_peer_name'   => false,
    'allow_self_signed'  => true,
  ),
);

