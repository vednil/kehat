#!/usr/bin/env bash

# Check IPv6 support
if [ $(cat /sys/module/ipv6/parameters/disable) -eq 1 ]
then
    # Disable IPv6 in Dovecot
    sed -i "s/#listen = \*, ::/listen = \*/g" /etc/dovecot/dovecot.conf
fi

# TLS certificate
if [ -z "$SSL_KEY" ] && [ -z "$SSL_CERT" ]
then
    if [ ! -f /etc/dovecot/private/$DOMAIN.key ] && [ ! -f /etc/dovecot/private/$DOMAIN.pem ]
    then
        # Create Self-Signed Certificate
        openssl req -nodes -x509 -newkey rsa:4096 -subj "/CN=$DOMAIN" -keyout /etc/dovecot/private/$DOMAIN.key -out /etc/dovecot/private/$DOMAIN.pem -days 3650
        sed -i "s/dovecot.pem/$DOMAIN.pem/g" /etc/dovecot/conf.d/10-ssl.conf
        sed -i "s/dovecot.key/$DOMAIN.key/g" /etc/dovecot/conf.d/10-ssl.conf
        chmod 700 /etc/dovecot/private
    fi
else
    # Use existing user certificate
    sed -i "s|ssl_cert = .*|ssl_cert = <$SSL_CERT|g" /etc/dovecot/conf.d/10-ssl.conf
    sed -i "s|ssl_key = .*|ssl_key = <$SSL_KEY|g" /etc/dovecot/conf.d/10-ssl.conf
fi

# Create DH key
if [ ! -f /etc/dovecot/private/dh.pem ]
then
    openssl dhparam -out /etc/dovecot/private/dh.pem 2048
fi

# DB parameters
sed -i "s/#connect =/connect = host=${DB_HOST} dbname=${DB_NAME} user=${DB_USER} password=${DB_PASSWORD}/g" /etc/dovecot/dovecot-sql.conf.ext

# Sieve
if [ ! -f /var/mail/vmail/sieve/default.sieve ]
then
    mkdir -p /var/mail/vmail/sieve
    echo -e "require [\"fileinto\"];\nif header :contains \"X-Spam-Flag\" \"YES\" {\n    fileinto \"Junk\";\n}" > /var/mail/vmail/sieve/default.sieve
    sievec /var/mail/vmail/sieve/default.sieve
fi

chown -R vmail:vmail /var/mail/vmail


exec "$@"
