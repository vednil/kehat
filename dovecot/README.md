# Quick reference

- Maintained by: https://gitlab.com/vednil/kehat

# What is it?

Dovecot is an open source mail delivery agent (MDA) that deliver mail to user mailbox and provide access to mailbox for user.

# How to use this image

Run Dovecot with self-signed certificate:

```
$ docker run -d \
        -p 110:110 \
        -p 143:143 \
        -p 993:993 \
        -p 995:995 \
        -e DOMAIN=mail.example.com \
        -e DB_HOST=db \
        -e DB_USER=postfix \
        -e DB_PASSWORD=password \
        -e DB_NAME=postfix \
        -v dovecot:/etc/dovecot/private  \
        -v vmail:/var/mail/vmail \
        vednil/dovecot
```

Note: A directory `/etc/dovecot/private` used to store self-signed certificate and Diffie-Hellman key. Certificate will be created at container startup.

Run Dovecot with your existing Let's Encrypt certificate:

```
$ docker run -d \
        -p 110:110 \
        -p 143:143 \
        -p 993:993 \
        -p 995:995 \
        -e SSL_KEY=/etc/letsencrypt/live/mail.example.com/privkey.pem \
        -e SSL_CERT=/etc/letsencrypt/live/mail.example.com/fullchain.pem \
        -e DB_HOST=db \
        -e DB_USER=postfix \
        -e DB_PASSWORD=password \
        -e DB_NAME=postfix \
        -v /etc/letsencrypt/live/mail.example.com:/etc/letsencrypt/live/mail.example.com:ro  \
        -v /etc/letsencrypt/archive/mail.example.com:/etc/letsencrypt/archive/mail.example.com:ro \
        -v vmail:/var/mail/vmail \
        vednil/dovecot sh -c 'while :; do sleep 6h & wait ${!}; doveconf 1>/dev/null && dovecot reload; done & dovecot -F'
```

Note: Dovecot don't get or renew Let's Encrypt certificate (e.g. you can use nginx and certbot to get and renew certificate). This image run with custom command to apply `dovecot reload` every 6 hours because it's required after renewing certificate.

**Note:** A directory `/var/mail/vmail` used as maibox storage. It's store all user mail.

## Environment Variables

### `SSL_KEY`

This optional environment variable is used to set `ssl_key` Dovecot parameter. Use this variable to use your existing certificate in Dovecot. Set path to your private key.

### `SSL_CERT`

This optional environment variable is used to set `ssl_cert` Dovecot parameter. Use this variable to use your existing certificate in Dovecot. Set path to your certificate chain.

### `DOMAIN`

This environment variable used to create self-signed certificate.

### `DB_HOST`

MariaDB IP address or DNS name. Database store domain and mailbox names.

### `DB_USER`

Database user name.

### `DB_PASSWORD`

Database user password.

### `DB_NAME`

Database name.

