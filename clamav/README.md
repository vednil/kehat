# Quick reference

- Maintained by: https://gitlab.com/vednil/kehat

# What is it?

ClamAV is an open source antivirus that check mail.

# How to use this image

```
$ docker run -d \
        -v amavis:/var/lib/amavis \
        -v clamav:/var/lib/clamav  \
        vednil/clamav
```

Note: A directory `/var/lib/clamav` used to store ClamAV virus database. This image don't update virus database (e.g. use `vednil/freshclam` image to automatically update virus database).

Note: Share a directory `/var/lib/amavis` with Amavis to check incoming mail. This image don't include Amavis (e.g. use `vednil/amavis` image).

