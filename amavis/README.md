# Quick reference

- Maintained by: https://gitlab.com/vednil/kehat

# What is it?

Amavis is an open source content filter for mail. Use this image to check spam and virus in mail.

# How to use this image

```
$ docker run -d \
        -e DOMAIN=example.com \
        -e POSTFIX_HOST=postfix \
        -e CLAMAV_HOST=clamav \
        -v amavis:/var/lib/amavis  \
        vednil/amavis sh -c 'sa-update.sh & amavisd-new foreground'
```

Note: This image run with custom command to update SpamAssassin rules every day.

Note: Share a directory `/var/lib/amavis` with ClamAV to check incoming mail. This image don't include ClamAV (e.g. use `vednil/clamav` image).

## Environment Variables

### `DOMAIN`

Mail domain.

### `POSTFIX_HOST`

Postfix IP address or DNS name.

### `CLAMAV_HOST`

ClamAV IP address or DNS name.

