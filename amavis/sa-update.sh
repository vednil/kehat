#!/usr/bin/env bash

while :
do
    sleep 1d
    wait $!
    sa-update -v
    if [ $? -eq 0 ]
    then
        sa-compile --quiet
        chown -R debian-spamd:debian-spamd /var/lib/spamassassin
        amavisd-new reload
    fi
done

