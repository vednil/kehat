#!/usr/bin/env bash

# Configure myhostname and mailname
sed -i "s/\$myhostname = .*/\$myhostname = '$DOMAIN';/g" /etc/amavis/conf.d/50-user
echo "$DOMAIN" > /etc/mailname

# ClamAV address
sed -i "s/'.*:3310'/'$(getent hosts $CLAMAV_HOST | awk '{print $1}'):3310'/g" /etc/amavis/conf.d/15-av_scanners
# Postfix address
sed -i "s/\$forward_method = 'smtp:\[.*\]:10025'/\$forward_method = 'smtp:\[$POSTFIX_HOST\]:10025'/g" /etc/amavis/conf.d/50-user

# Create amavis directories
mkdir -p /var/lib/amavis/db
mkdir -p /var/lib/amavis/tmp
mkdir -p /var/lib/amavis/virusmails
# Change amavis directories permissions
chmod 750 /var/lib/amavis/db
chmod 770 /var/lib/amavis/tmp
chmod 750 /var/lib/amavis/virusmails
# Change amavis home directory owner
chown -R amavis:amavis /var/lib/amavis

# Update SpamAssassin rules
sa-update -v && sa-compile --quiet
# Change debian-spamd home directory owner
chown -R debian-spamd:debian-spamd /var/lib/spamassassin

# Workaround for contianer restart
rm -f /var/run/amavis/amavisd.pid


exec "$@"
