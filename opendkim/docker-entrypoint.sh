#!/usr/bin/env bash

# Create DKIM key
if [ ! -f /var/db/dkim/${SELECTOR}.private ]
then
    echo "Create DKIM key..."
    opendkim-genkey -d ${DOMAIN} -s ${SELECTOR} -D /var/db/dkim
    echo "Key created. You must add below TXT record to your DNS:"
    cat /var/db/dkim/${SELECTOR}.txt
fi

chmod 700 /var/db/dkim
chown -R opendkim:opendkim /var/db/dkim

# Delete rsyslog pid file on container restart
rm -f /var/run/rsyslogd.pid


exec "$@"