FROM ubuntu:22.04

# Install Rsyslog with omstdout module
RUN apt-get update \
 && apt-get install -y ca-certificates gpg \
 && gpg --keyserver keyserver.ubuntu.com --recv-keys 0F6DD8135234BF2B \
 && gpg --output /etc/apt/trusted.gpg.d/adiscon-ubuntu-v8-stable.gpg --export 0F6DD8135234BF2B \
 && rm -rf /root/.gnupg \
 && echo "deb https://ppa.launchpadcontent.net/adiscon/v8-stable/ubuntu jammy main" | tee /etc/apt/sources.list.d/adiscon-ubuntu-v8-stable-jammy.list \
 && apt-get update \
 && apt-get install -y rsyslog rsyslog-omstdout \
 && rm -rf /var/lib/apt/lists/*

RUN sed -i 's/^$ModLoad imklog/#$ModLoad imklog/g' /etc/rsyslog.conf \
 && echo '$ModLoad omstdout' > /etc/rsyslog.d/50-default.conf \
 && echo '*.* :omstdout:' >> /etc/rsyslog.d/50-default.conf

# Install Supervisor
RUN apt-get update \
 && apt-get install -y supervisor \
 && rm -rf /var/lib/apt/lists/*

COPY supervisord.conf /etc/supervisor/supervisord.conf

# Install OpenDKIM
RUN apt-get update \
 && apt-get install -y opendkim opendkim-tools openssl \
 && rm -rf /var/lib/apt/lists/*

VOLUME /var/db/dkim

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 8891
CMD /usr/bin/supervisord