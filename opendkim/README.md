# Quick reference

- Maintained by: https://gitlab.com/vednil/kehat

# What is it?

OpenDKIM is a DKIM (Domain Keys Identified Mail) sign and verify software. Use to sign outgoing mail with your DKIM and check incoming mail.

# How to use this image

```
$ docker run -d \
        -e DOMAIN=example.com \
        -e SELECTOR=2022 \
        -v opendkim:/var/db/dkim  \
        vednil/opendkim
```

**Note 1:** A directory `/var/db/dkim` used to store DKIM that will be created at container startup if not exist.

**Note 2:** Add TXT record to your DNS from /var/db/dkim/${SELECTOR}.txt.

## Environment Variables

### `DOMAIN`

This environment variable is used to sign outgoing mail with DKIM from mail domain. Also it's used when generating DKIM if not exist.

### `SELECTOR`

This environment variable is used to sign outgoing mail with DKIM selector. Also it's used when generating DKIM if not exist.

